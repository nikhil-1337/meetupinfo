const express = require("express");
const bodyParser = require("body-parser");
const {
  dialogflow,
  Image,
  Permission,
  BasicCard,
  Button,
  List
} = require("actions-on-google");
const rq = require("request-promise");
const app = dialogflow();

var key = "514d6d3760136b4c68492259576c6678";

app.intent("Default Welcome Intent", conv => {
  conv.ask("Hi, how is it going?");
});
/**----------------1------------------- */
app.intent("request_permission", conv => {
  console.log("request_permission");
  const options = {
    context: "To address you by name and know your location",
    permissions: ["NAME", "DEVICE_PRECISE_LOCATION"]
  };
  conv.ask(new Permission(options));
});

app.intent("show meetups", async (conv, params, confirmationGranted) => {
  console.log("show meetups");

  if (confirmationGranted) {
    if (conv !== null && conv.data.meetupData === undefined) {
      conv.data.meetupData = [];
    }

    if (conv !== null && conv.data.meetupCount === undefined) {
      conv.data.meetupCount = 0;
    }
    await displayMeetup(conv);
  } else {
    conv.ask("Sorry, I could not figure out where you are.");
  }
});

async function displayMeetup(conv) {
  if (conv.data.meetupData.length === 0) {
    console.log("data.length === 0");
    await getMeetupData(conv);

    return buildSingleMeetupResponse(conv);
  } else {
    console.log("data.length === 0 else");
    return buildSingleMeetupResponse(conv);
  }
}

function getMeetupData(conv) {
  let latitude = conv.device.location.coordinates.latitude;
  let longitude = conv.device.location.coordinates.longitude;
  let url = `https://api.meetup.com/find/upcoming_events?&sign=true&photo-host=public&lon=${longitude}&page=30&lat=${latitude}&key=${key}`;
  return rq(url)
    .then(function(response) {
      let meetups = JSON.parse(response);
      if (meetups.hasOwnProperty("events")) {
        saveData(meetups.events, conv);
      }
    })
    .catch(function(err) {
      console.log("No meetups data");
      console.log(err);
    });
}

function saveData(data, conv) {
  if (conv !== null) {
    conv.data.meetupData = data;
  }
}

function buildSingleMeetupResponse(conv) {
  let responseToUser;
  let hasScreen =
    conv !== null &&
    conv.surface.capabilities.has("actions.capability.SCREEN_OUTPUT");
  let hasAudio =
    conv !== null &&
    conv.surface.capabilities.has("actions.capability.AUDIO_OUTPUT");
  console.log("buildSingleMeetupResponse");
  if (conv.data.meetupData.length === 0) {
    console.log("conv.data.length === 0");
    responseToUser = "No meetups available at this time!";
    conv.ask(responseToUser);
  } else if (conv.data.meetupCount < 0) {
    console.log("conv.data.meetupCount < 0");
    responseToUser = "No more meetups before this one!";
    conv.ask(responseToUser);
  } else if (conv.data.meetupCount < conv.data.meetupData.length) {
    let meetup = conv.data.meetupData[conv.data.meetupCount];

    responseToUser = " Meetup number " + (conv.data.meetupCount + 1) + " ";
    responseToUser += meetup.name;
    responseToUser += " by " + meetup.group.name;

    let date = new Date(meetup.time);
    responseToUser += " on " + date.toDateString() + ".";
    responseToUser += " Write or say next meetup to see more.";

    if (hasAudio) {
      let ssmlText =
        "<speak>" +
        ' <say-as interpret-as="ordinal">' +
        (conv.data.meetupCount + 1) +
        "</say-as> meetup. " +
        " Is " +
        meetup.name +
        '. <break time="1" />' +
        " By " +
        meetup.group.name +
        '. <break time="1" />' +
        " On " +
        date.toDateString() +
        '. <break time="1" />' +
        '<break time="600ms" />For more visit website. <break time="800ms" />' +
        " Say next meetup for more." +
        "</speak>";
      conv.ask(ssmlText.replace("&", " and "));
    } else {
      conv.ask(responseToUser);
    }

    if (hasScreen) {
      let image =
        "https://raw.githubusercontent.com/jbergant/udemydemoimg/master/meetup.png";
      conv.ask(
        new BasicCard({
          text: meetup.description,
          subtitle: "by " + meetup.group.name,
          title: meetup.name,
          buttons: new Button({
            title: "Read more",
            url: meetup.link
          }),
          image: new Image({
            url: image,
            alt: meetup.name
          }),
          display: "CROPPED"
        })
      );
    }
  }
  // return conv;
}

app.intent("show meetups - next", async conv => {
  console.log("show meetups - next");
  conv.data.meetupCount++;
  await displayMeetup(conv);
});

app.intent("show meetups - previous", async conv => {
  console.log("show meetups - previous");
  conv.data.meetupCount--;
  await displayMeetup(conv);
});

app.intent("show meetups - repeat", async conv => {
  console.log("show meetups - repeat");
  await displayMeetup(conv);
});

app.intent("request_grant", conv => {
  console.log("request_grant");
  const options = {
    context: "To address you by name and know your location",
    permissions: ["NAME", "DEVICE_PRECISE_LOCATION"]
  };
  conv.ask(new Permission(options));
});

app.intent("show meetup list", async (conv, params, confirmationGranted) => {
  console.log("sshow meetup list");

  if (confirmationGranted) {
    if (conv !== null && conv.data.meetupData === undefined) {
      conv.data.meetupData = [];
    }

    if (conv !== null && conv.data.meetupCount === undefined) {
      conv.data.meetupCount = 0;
    }
    await getMeetupList(conv);
  } else {
    conv.ask("Sorry, I could not figure out where you are.");
  }
});

async function getMeetupList(conv) {
  console.log("getMeetupList");

  conv.data.meetupCount = 0;
  if (conv.data.meetupData.length === 0) {
    await getMeetupData(conv);
    return buildMeetupListResponse(conv);
  } else {
    return buildMeetupListResponse(conv);
  }
}

function buildMeetupListResponse(conv) {
  let responseToUser;
  let hasScreen =
    conv !== null &&
    conv.surface.capabilities.has("actions.capability.SCREEN_OUTPUT");
  let hasAudio =
    conv !== null &&
    conv.surface.capabilities.has("actions.capability.AUDIO_OUTPUT");
  if (conv.data.meetupData.length === 0) {
    responseToUser = "No meetups available at this time!";
    conv.close(responseToUser);
  } else {
    let textList =
      "This is a list of meetups. Please select one of them to proceed";
    let ssmlText =
      "<speak>This is a list of meetups. " +
      'Please select one of them. <break time="1500ms" />';

    let image = "https://gitlab.com/nikhil-1337/meetupinfo/raw/master/logo-min.png";
    let items = {};
    for (let i = 0; i < conv.data.meetupData.length; i++) {
      let meetup = conv.data.meetupData[i];
      if (hasScreen) {
        items["meetup " + i] = {
          title: "meetup " + (i + 1),
          description: meetup.name,
          image: new Image({
            url: image,
            alt: meetup.name
          })
        };
      }
      responseToUser += " Meetup number " + (i + 1) + ":";
      responseToUser += meetup.name;
      responseToUser += " by " + meetup.group.name;
      let date = new Date(meetup.time);
      responseToUser += " on " + date.toDateString() + ". ";

      if (i < 3) {
        ssmlText +=
          '  <say-as interpret-as="ordinal">' +
          (i + 1) +
          "</say-as> meetup. " +
          '  <break time="500ms" />' +
          "Is " +
          meetup.name +
          '. <break time="700ms" />' +
          " On " +
          date.toDateString() +
          "." +
          ' For more information say "meetup ' +
          (i + 1) +
          '". <break time="1200ms" />';
      }
    }
    ssmlText += "</speak>";

    if (hasAudio) {
      conv.ask(ssmlText.replace("&", " and "));
    } else {
      conv.ask(textList);
      conv.ask(responseToUser);
    }

    if (hasScreen) {
      conv.ask(
        new List({
          title: "List of meetups: ",
          items
        })
      );
    }
  }
}

app.intent("show meetup list - select.number", async (conv, params, option) => {
  console.log("-----------------------------------");

  console.log(params);
  console.log("-----------------------------------");

  console.log("option", option);
  console.log("-----------------------------------");

  console.log(
    "conv.body.queryResult.parameters",
    conv.body.queryResult.parameters
  );

  if (!option) {
    conv.ask("You did not select any item from the list.");
  }
  if (option) {
    conv.data.meetupCount = parseInt(option.replace("meetup ", ""));
  }
  let number = params["number"];
  if (number.length > 0) {
    conv.data.meetupCount = parseInt(number[0]) - 1;
  }

  await displayMeetup(conv);
});



app.intent("show meetup list - next", async conv => {
  console.log("show meetup list - next");
  conv.data.meetupCount++;
  await displayMeetup(conv);
});

app.intent("show meetup list - previous", async conv => {
  console.log("show meetup list - previous");
  conv.data.meetupCount--;
  await displayMeetup(conv);
});

app.intent("show meetup list - repeat", async conv => {
  console.log("show meetup list - repeat");
  await displayMeetup(conv);
});


const expressApp = express().use(bodyParser.json());
expressApp.post("/", app);
expressApp.listen(3000, async () => {
  console.log("...magic...magic...");
});
